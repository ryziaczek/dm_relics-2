#  -*- coding: utf8 -*-
import json
import os.path
import codecs

import mysql.connector

my_user = 'root'
my_password = 'root'
my_host = '127.0.0.1'
json_path = "relics\\relics-register-json\\"


def create_database(name):
    cnx = mysql.connector.connect(user=my_user, password=my_password, host=my_host)
    cursor = cnx.cursor()
    try:
        cursor.execute("DROP DATABASE IF EXISTS " + name)
        cursor.execute(
            "CREATE DATABASE {} DEFAULT CHARACTER SET 'utf8'".format(name))
        print "CREATE DATABASE " + name
    except mysql.connector.Error as err:
        print("Failed creating database: {}".format(err))
        exit(1)

    cursor.close()
    cnx.close()


# show tables from db;
# select from * table;
def create_table_monument(cursor, table_name):
    cursor.execute("CREATE TABLE " + table_name + "(Id INT PRIMARY KEY AUTO_INCREMENT, "
                                                  "MyId INT, "
                                                  "nid_id INT,"
                                                  "identification VARCHAR(255) NULL ,"
                                                  "common_name VARCHAR(255) NULL ,"
                                                  "state ENUM('unchecked','checked','filled') ,"
                                                  "register_number VARCHAR(255) NULL, "

                                                  "dating_of_obj VARCHAR(255) NULL ,"
                                                  "street VARCHAR(255) NULL ,"
                                                  "latitude DOUBLE(9,6)  NULL , "
                                                  "longitude DOUBLE(9,6)  NULL , "

                                                  "fplace VARCHAR(255) NULL , "
                                                  "fprovince VARCHAR(255) NULL , "

                                                  "Name VARCHAR(25)"
                                                  ")")
    print "CREATE TABLE " + table_name


def insert_into_table_monument(cursor, table_name):
    value = "dupa"

    for i in range(7):
        _json = decode_json(i)

        if _json:
            cursor.execute(
                "INSERT INTO " + table_name + "(MyId,nid_id,identification,common_name,state,register_number,dating_of_obj,street,latitude,longitude,fplace,fprovince ) "
                                              "VALUES(%s, %s, %s, %s ,%s ,%s ,%s ,%s ,%s ,%s ,%s,%s )",
                (_json['id'],
                 _json['nid_id'],
                 _json['identification'],
                 _json['state'],
                 _json['common_name'],
                 _json['register_number'],
                 _json['dating_of_obj'],
                 _json['street'],
                 _json['latitude'],
                 _json['longitude'],
                 _json['fplace'],
                 _json['fprovince'],

                 ))
    print "INSERT INTO " + table_name


def insertIntoTableRelicRegister(cursor, table_name):
    numberOfJsons = getNumberOfJsons(json_path)
    namesOfJsons = getNamesOfJsons(json_path)

    print(numberOfJsons)

    for i in range(numberOfJsons - 1):
        if i % 1000 == 0:
            print("progress: " + str(i))

        _json = decodeJsonByName(namesOfJsons[i])

        # All Properties - >
        # [u'fprovince', u'descendants', u'place_id',
        #  u'street', u'country_code', u'common_name',
        #  u'fplace', u'register_number', u'id',
        #  u'voivodeship_name', u'place_name', u'commune_name',
        #  u'nid_id', u'dating_of_obj', u'longitude',
        #  u'state', u'identification', u'latitude',
        #  u'district_name']

        # trzeba sprawdzic czy record jesst unikalny!

        cursor.execute(
            "INSERT INTO " + table_name + "(MyId,nid_id,identification,common_name,state,register_number,dating_of_obj,street,latitude,longitude,fplace,fprovince ) "
                                          "VALUES(%s, %s, %s, %s ,%s ,%s ,%s ,%s ,%s ,%s ,%s,%s )",
            (_json['id'],
             _json['nid_id'],
             _json['identification'],
             _json['state'],
             _json['common_name'],
             _json['register_number'],
             _json['dating_of_obj'],
             _json['street'],
             _json['latitude'],
             _json['longitude'],
             _json['fplace'],
             _json['fprovince'],
             # dopisz reszte w bazie no i tutaj :)

             ))
    print "INSERT INTO " + table_name
    return


def get_table(cursor, table_name):
    cursor.execute("SELECT * FROM Writers")
    for row in cursor.fetchall():
        print row


def decode_json(nr):
    name = json_path + str(nr) + ".json"
    if os.path.isfile(name):
        with codecs.open(name) as data_file:
            data = json.load(data_file)
            data_file.close()

            return data


def decodeJsonByName(name):
    if os.path.isfile(name):
        with codecs.open(name) as data_file:
            data = json.load(data_file)
            data_file.close()

            return data


def getAllProperties():
    numberOfJson = getNumberOfJsons(json_path)
    namesOfJsons = getNamesOfJsons(json_path)

    _json = {}
    maxValueofKeys = 0
    properties = {}

    for i in range(numberOfJson - 1):
        _json = decodeJsonByName(namesOfJsons[i])
        properties.update(_json)

        # Zmienna dlugosc od 19 do 14 zauwazylem nic innego sie nie pojawialo
        # if len(_json) > maxValueofKeys :
        #     print( len(_json))
        #     maxValueofKeys = len(_json)
        # elif len(_json) < maxValueofKeys:
        #     print(len(_json))

        #Progress indicator
        if i % 1000 == 0:
            print("progress: " + str(i))

    print("max value of keys: " + str(maxValueofKeys))
    return properties.keys()


def getNamesOfJsons(path):
    namesOfJsons = []
    for file in os.listdir(path):
        if file.endswith(".json"):
            namesOfJsons.append(path + file)
    return namesOfJsons


def getNumberOfJsons(path):
    return len([name for name in os.listdir(path) if os.path.isfile(os.path.join(path, name))])

#
#
#
#Main
#
#
#

# print(getAllProperties())

db_name = "mama"
table_name = "Writers"

create_database(db_name)
cnx = mysql.connector.connect(user=my_user, password=my_password, host=my_host, database=db_name)
cursor = cnx.cursor()

create_table_monument(cursor, table_name)
insertIntoTableRelicRegister(cursor, table_name)
get_table(cursor, table_name)

cnx.commit()
cursor.close()
cnx.close()
